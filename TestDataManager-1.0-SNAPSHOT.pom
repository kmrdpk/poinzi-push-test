<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>io.nimbal</groupId>
    <artifactId>TestDataManager</artifactId>
    <packaging>jar</packaging>
    <name>Spring OSGi Bundle</name>
    <version>1.0-SNAPSHOT</version>
    <url>http://www.springframework.org/osgi</url>


    <description>API providing integration of existing Testing Datasource with Tree</description>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.0.3.RELEASE</version>
        <relativePath/> <!-- lookup parent from repository -->
    </parent>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <java.version>1.8</java.version>
    </properties>

    <dependencies>
        <!-- Spring Boot -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>

        <!-- Firebase Admin -->
        <dependency>
            <groupId>com.google.firebase</groupId>
            <artifactId>firebase-admin</artifactId>
            <version>6.2.0</version>
        </dependency>

        <!-- IBM Watson -->
        <dependency>
            <groupId>com.ibm.watson.developer_cloud</groupId>
            <artifactId>tone-analyzer</artifactId>
            <version>6.0.0</version>
        </dependency>

        <dependency>
            <groupId>com.ibm.watson.developer_cloud</groupId>
            <artifactId>speech-to-text</artifactId>
            <version>6.0.0</version>
        </dependency>

        <dependency>
            <groupId>com.ibm.watson.developer_cloud</groupId>
            <artifactId>personality-insights</artifactId>
            <version>6.1.0</version>
        </dependency>

        <!-- HTTP Client for other APIs -->
        <dependency>
            <groupId>org.apache.httpcomponents</groupId>
            <artifactId>httpclient</artifactId>
            <version>4.5.5</version>
        </dependency>

        <!-- Utility -->
        <dependency>
            <groupId>org.json</groupId>
            <artifactId>json</artifactId>
            <version>20180130</version>
        </dependency>

        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-io</artifactId>
            <version>1.3.2</version>
        </dependency>

        <!-- https://mvnrepository.com/artifact/com.trivago.rta/cucable-plugin -->
        <dependency>
            <groupId>com.trivago.rta</groupId>
            <artifactId>cucable-plugin</artifactId>
            <version>0.1.3</version>
        </dependency>

    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>


</project>